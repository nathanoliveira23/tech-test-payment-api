using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.Enums;

namespace tech_test_payment_api.Models
{
    public class Sale
    {
        [Key]
        public int Id { get; set; }
        public Seller Seller { get; set; }
        public DateTime Date { get; set; }
        public IList<Item> Items { get; set; }
        public EStatus Status { get; set; }
    }
}