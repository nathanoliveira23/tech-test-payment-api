namespace tech_test_payment_api.Enums
{
    public enum EStatus
    {
        PaymentAccept,
        SentToCarrier,
        Delivered,
        Cancel
    }
}