using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Utils
{
    public class ApiResponse
    {
        public string Message { get; set; }
        public int Status { get; set; }
    }
}