using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.Enums;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.DTO
{
    public class SaleDTO
    {
        public int SellerId { get; set; }
        public DateTime Date { get; set; }
        public IList<Item> Items { get; set; }
        public EStatus Status { get; set; }
    }
}