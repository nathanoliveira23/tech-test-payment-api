using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;
using tech_test_payment_api.Utils;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SellerController : ControllerBase
    {
        public SellerController(SaleContext context)
        {
            _context = context;
        }
        private readonly SaleContext _context;

        [HttpPost("RegisterSeller")]
        public async Task<IActionResult> RegisterSeller([FromBody] Seller seller)
        {
            // Verificação da existência de conteúdo no corpo da requisição
            if (String.IsNullOrEmpty(seller.Name) && 
                String.IsNullOrEmpty(seller.Email) && 
                String.IsNullOrEmpty(seller.CPF) && 
                String.IsNullOrEmpty(seller.Phone))
            {
                return BadRequest(new ApiResponse()
                {
                    Message = "É necessário informar todos os campos.",
                    Status = StatusCodes.Status400BadRequest
                });
            }

            // Verifica se o email informado no corpo da requisição já existe
            var sellerEmail = _context.Sellers.Any(x => x.Email == seller.Email);

            if (sellerEmail)
                return BadRequest(new ApiResponse()
                {
                    Message = "O email informado já existe.",
                    Status = StatusCodes.Status400BadRequest
                });

            try
            {
                await _context.Sellers.AddAsync(seller);
                await _context.SaveChangesAsync();

                return CreatedAtAction(nameof(GetSellerById), new { id = seller.Id }, seller);
            }
            catch (Exception ex)
            {
                return BadRequest(new ApiResponse()
                {
                    Message = $"Ocorreu um erro ao cadastrar um novo vendedor: {ex.Message}.",
                    Status = StatusCodes.Status500InternalServerError
                });
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetSellerById(int id)
        {
            try
            {
                var sellerId = await _context.Sellers.FindAsync(id);

                if (sellerId == null)
                    return NotFound(new ApiResponse()
                    {
                        Message = "Não foi possível encontrar o vendedor informado.",
                        Status = StatusCodes.Status404NotFound
                    });
            
                return Ok(sellerId);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new ApiResponse()
                {
                    Message = $"Ocorreu um erro ao buscar o vendedor: {ex.Message}.",
                    Status = StatusCodes.Status500InternalServerError
                });
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateSeller([FromBody] Seller seller, int id)
        {
            try
            {
                var dbSeller = await _context.Sellers.FindAsync(id);

                if (dbSeller != null)
                {
                    dbSeller.Name = seller.Name ?? dbSeller.Name;
                    dbSeller.Email = seller.Email ?? dbSeller.Email;
                    dbSeller.CPF = seller.CPF ?? dbSeller.CPF;
                    dbSeller.Phone = seller.Phone ?? dbSeller.Phone;

                    _context.Sellers.Update(dbSeller);
                    await _context.SaveChangesAsync();

                    return NoContent();
                }
                else
                {
                    return NotFound(new ApiResponse()
                    {
                        Message = "Não foi possível encontrar o vendedor informado.",
                        Status = StatusCodes.Status404NotFound
                    });
                }
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new ApiResponse()
                {
                    Message = $"Ocorreu um erro ao atualizar os dados do vendedor: {ex.Message}.",
                    Status = StatusCodes.Status500InternalServerError
                });
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSeller(int id)
        {
            var sellerId = await _context.Sellers.FindAsync(id);
            
            if (sellerId == null)
                return NotFound(new ApiResponse()
                {
                    Message = "Não foi possível encontrar o vendedor informado.",
                    Status = StatusCodes.Status404NotFound
                });

            try
            {
                _context.Sellers.Remove(sellerId);
                await _context.SaveChangesAsync();

                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new ApiResponse()
                {
                    Message = $"Ocorreu um erro ao deletar os dados do vendedor: {ex.Message}.",
                    Status = StatusCodes.Status500InternalServerError
                });
            }
        }
    }
}