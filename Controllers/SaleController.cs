using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.DTO;
using tech_test_payment_api.Enums;
using tech_test_payment_api.Models;
using tech_test_payment_api.Utils;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SaleController : ControllerBase
    {
        public SaleController(SaleContext context)
        {
            _context = context;
        }
        private readonly SaleContext _context;

        [HttpPost("RegisterSale/{id}")]
        public async Task<IActionResult> RegisterSale([FromBody] SaleDTO saleDto, int id)
        {
            var sellerId = await _context.Sellers.FindAsync(id);

            if (sellerId == null)
                return NotFound(new ApiResponse()
                {
                    Message = "Não foi possível encontrar o vendedor informado.",
                    Status = StatusCodes.Status404NotFound
                });
            
            // Verifica se o status informado na requisição está presente no enum de status
            if (saleDto.Status != EStatus.PaymentAccept || 
                saleDto.Status != EStatus.SentToCarrier ||
                saleDto.Status != EStatus.Delivered ||
                saleDto.Status != EStatus.Cancel)
            {
                return BadRequest(new ApiResponse()
                {
                    Message = "É preciso informar um status de venda entre 0 e 4.",
                    Status = StatusCodes.Status400BadRequest
                });
            }
            
            try 
            {
                Sale sales = new Sale()
                {
                    Seller = sellerId,
                    Items = saleDto.Items,
                    Status = saleDto.Status
                };

                await _context.Sales.AddAsync(sales);
                await _context.SaveChangesAsync();

                return CreatedAtAction(nameof(GetSaleById), new { id = saleDto.SellerId }, sales);
            }
            catch (Exception ex)
            {
                return BadRequest(new ApiResponse()
                {
                    Message = $"Ocorreu um erro ao cadastrar um nova venda: {ex.Message}.",
                    Status = StatusCodes.Status500InternalServerError
                });
            }
        }

        [HttpGet("{id}")]
        public IActionResult GetSaleById(int id)
        {
            try
            {
                var saleInfo = _context.Sales
                    .Where(saleId => saleId.Id == id)
                    .Select(x => new { x.Id, x.Seller, x.Items, x.Status });

                return Ok(saleInfo);
            }
            catch (Exception ex)
            {
                return BadRequest(new ApiResponse()
                {
                    Message = $"Ocorreu um erro ao buscar as informações da venda: {ex.Message}.",
                    Status = StatusCodes.Status500InternalServerError
                });
            }
        }

        [HttpPatch("{id}")]
        public async Task<IActionResult> UpdateStatus([FromBody] JsonPatchDocument<Sale> patch, int id)
        {
            var saleId = await _context.Sales.FindAsync(id);

            if (saleId == null)
                return NotFound(new ApiResponse()
                {
                    Message = "Não foi possível encontrar a venda informada.",
                    Status = StatusCodes.Status404NotFound
                });
            
            try
            {
                patch.ApplyTo(saleId);
                await _context.SaveChangesAsync();

                return Ok(saleId);
            }
            catch (Exception ex)
            {
                return BadRequest(new ApiResponse()
                {
                    Message = $"Ocorreu um erro ao atualiza o status da venda: {ex.Message}.",
                    Status = StatusCodes.Status500InternalServerError
                });
            }
        }
    }
}